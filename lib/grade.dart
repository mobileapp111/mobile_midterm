import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GradePage extends StatelessWidget {
  const GradePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ผลการศึกษา'),
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Colors.black,
        ),
      ),
      body: Container(
        color: Colors.grey.shade200,
        child: ListView(
          children: [
            Container(
              width: 100,
              height: 200,
              color: Colors.grey.shade200,
              margin: EdgeInsets.all(8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Image.network(
                          'https://www.i-pic.info/i/Aqak357581.jpg',
                          width: 150,
                          height: 200,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 13, left: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text(
                                'รหัสนักศึกษา',
                                style: TextStyle(fontSize: 16, color: Colors.grey),
                              ),
                            ),
                            Container(
                              child: Text(
                                '63160197',
                                style: TextStyle(fontSize: 16, color: Colors.black),
                              ),
                            ),
                            Container(
                              child: Text(
                                'ชื่อ',
                                style: TextStyle(fontSize: 16, color: Colors.grey),
                              ),
                            ),
                            Container(
                              child: Text(
                                'นางสาว ธนทญา วรทญานันทน์',
                                style: TextStyle(fontSize: 16, color: Colors.black),
                              ),
                            ),
                            Container(
                              child: Text(
                                'คณะ',
                                style: TextStyle(fontSize: 16, color: Colors.grey),
                              ),
                            ),
                            Container(
                              child: Text(
                                'คณะวิทยาการสารสนเทศ',
                                style: TextStyle(fontSize: 16, color: Colors.black),
                              ),
                            ),
                            Container(
                              child: Text(
                                'สาขา',
                                style: TextStyle(fontSize: 16, color: Colors.grey),
                              ),
                            ),
                            Container(
                              child: Text(
                                'วท.บ. (วิทยาการคอมพิวเตอร์)',
                                style: TextStyle(fontSize: 16, color: Colors.black),
                              ),
                            ),
                          ],
                        ),
                      )

                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 350,
              height: 100,
              margin: EdgeInsets.all(15),
              decoration: ShapeDecoration(
                color: Colors.amber.shade50,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Text(
                        'เกรดปัจจุบัน',
                        style:
                            TextStyle(fontSize: 18, color: Colors.amberAccent),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Text(
                            '2.99',
                            style: TextStyle(
                                fontSize: 30, color: Colors.amberAccent),
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Text(
                            '(อัปเดตล่าสุด 31 มีนาคม 2566)',
                            style: TextStyle(
                                fontSize: 18, color: Colors.amberAccent),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Container(
              width: 30,
              height: 30,
              child: Center(
                child: Container(
                  child: Text(
                    'ภาคการเรียน : 1/2565',
                    style: TextStyle(fontSize: 25, ),
                  ),
                ),
              ),
            ),
            Container(
              height: 360,
              width: 300,
              child: DataTable(
                columns: [
                  DataColumn(
                    label: Text(
                      'รหัสวิชา',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'หน่วยกิต',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'ระดับ',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
                rows: [
                  DataRow(
                    cells: [
                      DataCell(Text('88633159')),
                      DataCell(Text('3')),
                      DataCell(Text('C+')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('88631159')),
                      DataCell(Text('3')),
                      DataCell(Text('B')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('88635359')),
                      DataCell(Text('3')),
                      DataCell(Text('B')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('88636159')),
                      DataCell(Text('3')),
                      DataCell(Text('D+')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('88634159')),
                      DataCell(Text('3')),
                      DataCell(Text('B+')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('88624259')),
                      DataCell(Text('3')),
                      DataCell(Text('C+')),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 350,
              height: 70,
              margin: EdgeInsets.all(5),
              decoration: ShapeDecoration(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
              child: ListView(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Center(
                              child: Container(
                                child: Text(
                                  'หน่วยกิตสะสม 36',
                                  style: TextStyle(fontSize: 25, ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Center(
                              child: Container(
                                child: Text(
                                  'เกรดเฉลี่ย 2.67',
                                  style: TextStyle(fontSize: 20, color: Colors.grey ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 30,
              height: 30,
              child: Center(
                child: Container(
                  child: Text(
                    'ภาคการเรียน : 2/2565',
                    style: TextStyle(fontSize: 25, ),
                  ),
                ),
              ),
            ),
            Container(
              height: 360,
              width: 300,
              child: DataTable(
                columns: [
                  DataColumn(
                    label: Text(
                      'รหัสวิชา',
                      style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'หน่วยกิต',
                      style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'ระดับ',
                      style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
                rows: [
                  DataRow(
                    cells: [
                      DataCell(Text('88624559')),
                      DataCell(Text('3')),
                      DataCell(Text('N/A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('88646259')),
                      DataCell(Text('3')),
                      DataCell(Text('N/A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('88634459')),
                      DataCell(Text('3')),
                      DataCell(Text('N/A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('88634259')),
                      DataCell(Text('3')),
                      DataCell(Text('N/A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('88624359')),
                      DataCell(Text('3')),
                      DataCell(Text('N/A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('88624459')),
                      DataCell(Text('3')),
                      DataCell(Text('N/A')),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 350,
              height: 70,
              margin: EdgeInsets.all(5),
              decoration: ShapeDecoration(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
              child: ListView(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Center(
                              child: Container(
                                child: Text(
                                  'หน่วยกิตสะสม 18',
                                  style: TextStyle(fontSize: 25, ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Center(
                              child: Container(
                                child: Text(
                                  'เกรดเฉลี่ย 0.00',
                                  style: TextStyle(fontSize: 20, color: Colors.grey ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),

          ],
        ),
      ),


    );
  }
}
