import 'package:flutter/material.dart';

class ScheduleTable extends StatelessWidget {
  const ScheduleTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ตารางเรียนนิสิต'),
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Colors.black,
        ),
      ),
      body: ListView(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text(
                          "ตารางเรียนนิสิต ปีการศึกษา 2565",
                          style: TextStyle(fontSize: 20.0),
                        ),
                        Text(""),
                      ],
                    ),
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            color: Colors.amber.shade50,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("จันทร์"),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("อังคาร"),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("พุธ",style: TextStyle(color: Colors.red),),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("พฤหัสบดี"),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("ศุกร์"),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("เสาร์"),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("อาทิตย์"),
                        Text(""),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          schedule1(),
          schedule2(),
          schedule3(),
        ],
      ),
    );
  }
}

class schedule1 extends StatelessWidget {
  const schedule1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              LineSchedule(
                lines: [0.0, 40.0, 40.0, 0.0],
              ),
            ],
          ),
        ),
        SizedBox(
          width: 10.0,
        ),
        Expanded(
          child: Container(
            height: 130.0,
            child: Container(
              margin: EdgeInsets.only(left: 4.0),
              color: Colors.green.shade200,
              padding: EdgeInsets.only(left: 16.0, top: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 21.0,
                    child: Row(
                      children: <Widget>[
                        Text("10:00-12:00"),
                        VerticalDivider(),
                        Text("88634459-59"),
                        VerticalDivider(),
                        Text("IF-4C02,IF"),
                      ],
                    ),
                  ),
                  Text(""),
                  Text(
                    "การพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1",
                    style:
                        TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}

class schedule2 extends StatelessWidget {
  const schedule2({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              LineSchedule(
                lines: [0.0, 40.0, 40.0, 0.0],
              ),
            ],
          ),
        ),
        SizedBox(
          width: 12.0,
        ),
        Expanded(
          child: Container(
            height: 130.0,
            child: Container(
              margin: EdgeInsets.only(left: 4.0),
              color: Colors.green.shade200,
              padding: EdgeInsets.only(left: 16.0, top: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 21.0,
                    child: Row(
                      children: <Widget>[
                        Text("13:00 - 15:00"),
                        VerticalDivider(),
                        Text("88634259-59"),
                        VerticalDivider(),
                        Text("IF-4C01, IF"),
                      ],
                    ),
                  ),
                  Text(""),
                  Text(
                    "การโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม",
                    style:
                        TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}

class schedule3 extends StatelessWidget {
  const schedule3({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              LineSchedule(
                lines: [0.0, 40.0, 40.0, 0.0],
              ),
            ],
          ),
        ),
        SizedBox(
          width: 12.0,
        ),
        Expanded(
          child: Container(
            height: 100.0,
            child: Container(
              margin: EdgeInsets.only(left: 4.0),
              color: Colors.green.shade200,
              padding: EdgeInsets.only(left: 16.0, top: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 21.0,
                    child: Row(
                      children: <Widget>[
                        Text("15:00 - 17:00"),
                        VerticalDivider(),
                        Text("88624359-59"),
                        VerticalDivider(),
                        Text("IF-3C01, IF"),
                      ],
                    ),
                  ),
                  Text(""),
                  Text(
                    "การเขียนโปรแกรมบนเว็บ",
                    style:
                        TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}

class LineSchedule extends StatelessWidget {
  final lines;

  const LineSchedule({
    Key? key,
    this.lines,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: List.generate(
        4,
        (index) => Container(
          height: 2.0,
          width: lines[index],
          color: Color(0xffd0d2d8),
          margin: EdgeInsets.symmetric(vertical: 14.0),
        ),
      ),
    );
  }
}
