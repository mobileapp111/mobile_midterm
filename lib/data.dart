
import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

class DataPage extends StatelessWidget {
  const DataPage({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ข้อมูลของฉัน'),
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Colors.black,
        ),
      ),
      body: Container(
        color: Colors.grey.shade200,
        child: ListView(
          children: [
            Container(
              width: 100,
              height: 100,
              color: Colors.grey.shade200,
              margin: EdgeInsets.all(8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Image.network(
                          'https://www.i-pic.info/i/Aqak357581.jpg',
                          width: 100,
                          height: 100,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Text(
                              'ธนทญา วรทญานันทน์',
                              style: TextStyle(fontSize: 20),
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            child: Text(
                              'คณะวิทยาการสารสนเทศ',
                              style:
                                  TextStyle(fontSize: 16, color: Colors.grey),
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            child: Text(
                              'นิปปอน',
                              style:
                                  TextStyle(fontSize: 16, color: Colors.black),
                            ),
                          )
                        ],
                      ),
                    ],
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              width: 350,
              height: 300,
              margin: EdgeInsets.all(15),
              decoration: ShapeDecoration(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        child: Text(
                          '  ข้อมูลส่วนบุคคล',
                          style: TextStyle(fontSize: 20, color: Colors.amber),
                        ),
                      )
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Text(
                              ' ชื่อ (อังกฤษ)',
                              style:
                                  TextStyle(fontSize: 16, color: Colors.grey),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' THANATAYA WORATAYANUN',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' ชื่อ (ไทย)',
                          style: TextStyle(fontSize: 16, color: Colors.grey),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' ธนทญา วรทญานันทน์',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' วันเกิด',
                          style: TextStyle(fontSize: 16, color: Colors.grey),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' 05 ตุลาคม 2544',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' เพศ',
                          style: TextStyle(fontSize: 16, color: Colors.grey),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' หญิง',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' สัญชาติ',
                          style: TextStyle(fontSize: 16, color: Colors.grey),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' ไทย',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              width: 350,
              height: 300,
              margin: EdgeInsets.all(15),
              decoration: ShapeDecoration(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        child: Text(
                          '  หน่วยงาน',
                          style: TextStyle(fontSize: 20, color: Colors.amber),
                        ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Text(
                              ' ประเภทผู้ใช้งาน',
                              style:
                                  TextStyle(fontSize: 16, color: Colors.grey),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' นักศึกษา',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' รหัสประจำตัว',
                          style: TextStyle(fontSize: 16, color: Colors.grey),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' 63160197',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' ชั้นปี',
                          style: TextStyle(fontSize: 16, color: Colors.grey),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' 3',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' คณะ(ไทย)',
                          style: TextStyle(fontSize: 16, color: Colors.grey),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' คณะวิทยาการสารสนเทศ',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' อีเมล',
                          style: TextStyle(fontSize: 16, color: Colors.grey),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' 63160197@go.buu.ac.th',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              width: 350,
              height: 250,
              margin: EdgeInsets.all(15),
              decoration: ShapeDecoration(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        child: Text(
                          '  ข้อมูลติดต่อ',
                          style: TextStyle(fontSize: 20, color: Colors.amber),
                        ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Text(
                              ' อีเมล',
                              style:
                                  TextStyle(fontSize: 16, color: Colors.grey),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' nniporii.nn@gmail.com',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' ที่อยู่',
                          style: TextStyle(fontSize: 16, color: Colors.grey),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' 36/16 ถ.เอมอรอุทิศ 2 ต.หน้าเมือง \n อ.เมือง จ.ฉะเชิงเทรา',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' เบอร์โทร',
                          style: TextStyle(fontSize: 16, color: Colors.grey),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          ' 0624289433',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
