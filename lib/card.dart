import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  const CardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('มหาวิทยาลัยบูรพา'),
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Colors.black,
        ),
      ),
      body: Container(
        color: Colors.grey.shade200,
        child: Center(
          child: Image.network('https://www.i-pic.info/i/LHT5357591.jpg',width: 400,height: 500,),
        ),
        alignment: Alignment.center
        ),


    );
  }
}
