import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:mobile_midterm/menu.dart';
import 'package:mobile_midterm/data.dart';
import 'package:mobile_midterm/grade.dart';
import 'package:mobile_midterm/scheduleTable.dart';

import 'login.dart';

void main() {
  runApp(const ColumnExample());
}

class ColumnExample extends StatelessWidget {
  const ColumnExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'ระบบบริการการศึกษา',
        theme: ThemeData(
          primarySwatch: Colors.amber,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text('ระบบการศึกษา มหาวิทยาลัยบูรพา'),
          ),
          body: const SafeArea(
            child: LoginPage(),
          ),
        ),
      ),
    );
  }
}


