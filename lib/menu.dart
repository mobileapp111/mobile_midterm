import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:mobile_midterm/login.dart';
import 'package:mobile_midterm/data.dart';
import 'package:mobile_midterm/card.dart';
import 'package:mobile_midterm/grade.dart';
import 'package:mobile_midterm/scheduleTable.dart';

class MenuPage extends StatelessWidget {
  const MenuPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('มหาวิทยาลัยบูรพา'),
        
      ),
      body: Container(
          color: Colors.grey.shade200,
          height: 800,
          child: ListView(
            children: [
              Container(
                  width: 350,
                  height: 200,
                  margin: EdgeInsets.only(top: 1),
                  child: Image.network(
                    "https://www.i-pic.info/i/rAfD356670.png",
                    fit: BoxFit.cover,
                  )),
              Divider(),
              Container(
                width: 350,
                height: 60,
                margin: EdgeInsets.only(top: 4),
                color: Colors.grey.shade200,
                child: dayList(),
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        width: 390,
                        height: 30,
                        color: Colors.amber,
                        alignment: Alignment.center,
                        child: Text(
                          "ยินดีต้อนรับสู่ระบบบริการการศึกษา",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.only(
                              top: 25, left: 15, right: 15, bottom: 15),
                          width: 350,
                          height: 40,
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Icon(Icons.person),
                                SizedBox(width: 8),
                                Text(
                                  "ข้อมูลของฉัน",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const DataPage()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          width: 350,
                          height: 40,
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Icon(Icons.hourglass_bottom),
                                SizedBox(width: 8),
                                Text(
                                  "ผลการศึกษา",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const GradePage()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          width: 350,
                          height: 40,
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Icon(Icons.credit_card),
                                SizedBox(width: 8),
                                Text(
                                  "บัตรนิสิต",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const CardPage()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          width: 350,
                          height: 40,
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Icon(Icons.library_books),
                                SizedBox(width: 8),
                                Text(
                                  "ตารางเรียน",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const ScheduleTable()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.all(15),
                        width: 350,
                        height: 40,
                        child: ElevatedButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return LoginPage();
                              }));
                            },
                            child: const Text(
                              'ออกจากระบบ',
                              style: TextStyle(fontSize: 18),
                            )),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          )),
    );
  }
}

Widget dayList() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildDay1(),
      buildDay2(),
      buildDay3(),
      buildDay4(),
      buildDay5(),
      buildDay6(),
      buildDay7()
    ],
  );
}

Widget buildDay1() {
  return Column(
    children: <Widget>[
      Text(
        "Mon",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
      Text(
        "30",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
      Text(
        "Jan",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
    ],
  );
}

Widget buildDay2() {
  return Column(
    children: <Widget>[
      Text(
        "Tue",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
      Text(
        "31",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
      Text(
        "Jan",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
    ],
  );
}

Widget buildDay3() {
  return Column(
    children: <Widget>[
      Text(
        "Wed",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
      Text(
        "1",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
      Text(
        "Feb",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
    ],
  );
}

Widget buildDay4() {
  return Column(
    children: <Widget>[
      Text(
        "Thu",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
      Text(
        "2",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
      Text(
        "Feb",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
    ],
  );
}

Widget buildDay5() {
  return Column(
    children: <Widget>[
      Text(
        "Fri",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
      Text(
        "3",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
      Text(
        "Feb",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
    ],
  );
}

Widget buildDay6() {
  return Column(
    children: <Widget>[
      Text(
        "Sat",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
      Text(
        "4",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
      Text(
        "Feb",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
    ],
  );
}

Widget buildDay7() {
  return Column(
    children: <Widget>[
      Text(
        "Sun",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
      Text(
        "5",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
      Text(
        "Feb",
        style: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
      ),
    ],
  );
}
